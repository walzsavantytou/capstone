import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/LandingPage.vue";
import req from "../components/PendingReqPage.vue";
import update from "../components/UpdatePage.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/req",
    name: "req",
    component: req,
  },
  {
    path: "/update",
    name: "update",
    component: update,
  },
  {
    path: "/signup",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../components/SignUpPage.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
